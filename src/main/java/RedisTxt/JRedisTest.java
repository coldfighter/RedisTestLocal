package RedisTxt;

import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2019/3/16.
 */
public class JRedisTest {
    public static void main(String[] args) {
//        直接创建
        Jedis jedis=new Jedis("192.168.91.129",6379);
        jedis.set("name","tony");
        System.out.print(jedis.get("name"));
        jedis.close();

        
 //创建redis池配置
            JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
            jedisPoolConfig.setMaxTotal(100);
            List<JedisShardInfo> jedisShardInfoList=new ArrayList<JedisShardInfo>();
            JedisShardInfo jedisShardInfo1=new JedisShardInfo("192.168.91.129",6379);
            JedisShardInfo jedisShardInfo2=new JedisShardInfo("192.168.91.129",6380);
            jedisShardInfoList.add(jedisShardInfo1);
            jedisShardInfoList.add(jedisShardInfo2);
            ShardedJedisPool pool=new ShardedJedisPool(jedisPoolConfig,jedisShardInfoList);
            ShardedJedis jedis=pool.getResource();
            System.out.print(jedis.get("name"));
            for(int i=0;i<10;i++){
                jedis.set("name"+i,""+i);
            }

    }
}
